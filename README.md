# Projet J2EE - VIROLAN Jonathan

## Objectifs 
Réaliser une application J2EE (Java Enterpise Edition) tout en utilisant les framework Hibernate (pour la persistance des données/objets dans une base de donnée, back-end) et spring boot (pour le coté front-end).

## Installation du Projet
Se positionner dans le dossier de votre chois puis lancer en ligne de commande la commande suivante :

    git clone https://gitlab.com/virolan.jon/projet_j2ee.git

Puis placez vous dans le dossier avec cette ligne de commande :

    cd projet_j2ee

## Installation des Dépendances
Depuis le répertoire du projet, utilisez ces lignes de commandes pour pour voir installer les premières dépendances (openjdk-11 et postgresql) et créer la base de donnée permettant le bon fonctionnement de mon projet :

   

    bash install_dependencies.sh
    sudo -i -u postgres
    psql
    Create user formation
    alter role formation with createdb
    create database formation owner formation
    alter user test with encrypted password 'formation20192020
    \q

## Lancement du projet
Pour lancer le projet, 2 possibilité s'offre à vous pour lancer le projet :

 1. Via le fichier shell se trouvant dans le dossier du projet. Il suffit de d'executer le fichier shell via cette ligne de commande :

    bash launch_projetj2ee.sh

 2. Via un IDE (eclipse, netbeans, ...), il doit supporter les projet Java via Maven. Il suffit de lancer l'IDE, de votre choix, puis d'ouvrir le projet 'MiniProjetVirolanJonathan' et de le run.

## Utilisation du Projet
Pour finir, il suffit de vous rendre sur la page suivante pour utiliser le projet et d'y naviguer librement :
https://localhost:8080/

Le login et le mot de passe pour se connecter est : admin
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE5NjA2NzE3MDNdfQ==
-->