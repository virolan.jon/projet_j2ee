/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.MiniProjetVirolanJonathan;

/**
 *
 * @author formation
 */
public class EvenementForm {
    
    private String intitule;
    private String theme;
    private String datedebut;
    private String duree;
    private int nbpartmax;
    private String description;
    private String organisateur;
    private String typeeven;
    
    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(String datedebut) {
        this.datedebut = datedebut;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public int getNbpartmax() {
        return nbpartmax;
    }

    public void setNbpartmax(int nbpartmax) {
        this.nbpartmax = nbpartmax;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganisateur() {
        return organisateur;
    }

    public void setOrganisateur(String organisateur) {
        this.organisateur = organisateur;
    }

    public String getTypeeven() {
        return typeeven;
    }

    public void setTypeeven(String typeeven) {
        this.typeeven = typeeven;
    }  
}
