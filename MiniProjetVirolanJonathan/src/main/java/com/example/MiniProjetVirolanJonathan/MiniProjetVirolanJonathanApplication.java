package com.example.MiniProjetVirolanJonathan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniProjetVirolanJonathanApplication {

    public static void main(String[] args) {

        SpringApplication.run(MiniProjetVirolanJonathanApplication.class, args);
    }

}
