/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.MiniProjetVirolanJonathan;

import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;

/**
 *
 * @author formation
 */
@Controller
@SessionAttributes("connexion")
public class MainController {   
    
    @Autowired
    private ParticipantRepository participantRepository;
    
    @Autowired
    private EvenementRepository evenementRepository;
    
    @GetMapping("/connexion")
    public String showIndexPage(Model model){
        ConnexionForm c = new ConnexionForm();
        model.addAttribute("connexionForm", c);
        return "connexion";
    }
    
    @PostMapping("/connexion")
    public String connexion(@ModelAttribute("connexionForm") ConnexionForm connexionForm, HttpSession session){
        
        String login = connexionForm.getLogin();
    	String mdp = connexionForm.getMdp();
    	if (login.equals("admin") && mdp.equals("admin")) {
            if (evenementRepository.findById(1).isEmpty()){
                Evenement e = new Evenement("Match de Football","Sport", "2020-03-01","1 heure",50,"Arsenal-Chelsea","L'équipe d'arsenal","Sportif");
                Participant p = new Participant("Virolan","Jonathan","jonathan.virolan@ensg.eu","1997-02-21","Rien","Rien",e);
                evenementRepository.save(e);
                participantRepository.save(p); 
            }
            session.setAttribute("connexion", "Done");
            return "redirect:/Accueil.html";
    	}
        return "connexion";
    }
    
    @GetMapping("/Accueil")
    public String Accueil(HttpSession session){
        if (session.getAttribute("connexion").equals("Done")){
            return "Accueil";
        }
        return "redirect:/connexion";
    }
    
    @GetMapping("/Accueil.html/participants")
    public String getAllParticipants(Model model, HttpSession session) {
        if (session.getAttribute("connexion").equals("Done")){
            Iterable<Participant> allParticipants = participantRepository.findAll();
            model.addAttribute("allParticipants", allParticipants);
            return "participants";
        }
        return "redirect:/connexion";
    } 
    
    @GetMapping("/Accueil.html/ajoutparticipant")
    public String showAjoutParticipantPage(Model model, HttpSession session) {
 
        if (session.getAttribute("connexion").equals("Done")){
            ParticipantForm participantForm = new ParticipantForm();
            model.addAttribute("participantForm", participantForm);

            Iterable<Evenement> allEvenements = evenementRepository.findAll();
            model.addAttribute("allEvenements", allEvenements);
            return "ajoutparticipant";
        }
        return "redirect:/connexion";
    }
    
    @PostMapping("/Accueil.html/ajoutparticipant")
    public String ajoutParticipant (@ModelAttribute("participantForm") ParticipantForm participantForm) {
        Evenement e  = evenementRepository.findById(participantForm.getNumeven()).get();
        Participant p = new Participant(participantForm.getNom(),
                participantForm.getPrenom(),
                participantForm.getEmail(),
                participantForm.getDatenaiss(),
                participantForm.getObservation(),
                participantForm.getOrganisation(),
                e);
        e.addParticipant(p);
        participantRepository.save(p);
        evenementRepository.save(e);
        return "redirect:/Accueil.html/participants";
    }
    
    @GetMapping("/Accueil.html/supparticipant")
    public String showSuppParticipantPage(Model model, HttpSession session){
        
        if (session.getAttribute("connexion").equals("Done")){
            Participant p = new Participant();
            model.addAttribute("participant",p);
            model.addAttribute("allParticipants", participantRepository.findAll());
            return "supparticipant";
        }
        return "redirect:/connexion";
    }
    
    @PostMapping("/Accueil.html/supparticipant")
    public String suppParticipant(@ModelAttribute("participant") Participant participant){
        participantRepository.deleteById(participant.getNumpers());
        return "redirect:/Accueil.html/participants";
    }
    
    @GetMapping("/Accueil.html/evenements")
    public String getAllEvenements(Model model,HttpSession session) {
        if (session.getAttribute("connexion").equals("Done")){
            Iterable<Evenement> allEvenements = evenementRepository.findAll();
            model.addAttribute("allEvenements", allEvenements); 
            return "evenements";
        }
        return "redirect:/connexion";
    }
    
    @GetMapping("/Accueil.html/ajoutevenement")
    public String showAjoutEvenementPage(Model model, HttpSession session) {
        
        if (session.getAttribute("connexion").equals("Done")){
            EvenementForm evenementForm = new EvenementForm();
            model.addAttribute("evenementForm", evenementForm);
            return "ajoutevenement";
        }
        return "redirect:/connexion";
    }
    
    @PostMapping("/Accueil.html/ajoutevenement")
    public String ajoutEvenement (@ModelAttribute("evenementForm") EvenementForm evenementForm) {
        Evenement e = new Evenement(evenementForm.getIntitule(),
                evenementForm.getTheme(),
                evenementForm.getDatedebut(),
                evenementForm.getDuree(),
                evenementForm.getNbpartmax(),
                evenementForm.getDescription(),
                evenementForm.getOrganisateur(),
                evenementForm.getTypeeven());
        evenementRepository.save(e);
        return "redirect:/Accueil.html/evenements";
    }
    
    @GetMapping("/Accueil.html/supevenement")
    public String showSuppEvenementPage(Model model, HttpSession session){
        
        if (session.getAttribute("connexion").equals("Done")){
            Evenement e = new Evenement();
            model.addAttribute("evenement",e);
            model.addAttribute("allEvenements", evenementRepository.findAll());
            return "supevenement";
        }
        return "redirect:/connexion";
    }
    
    @PostMapping("/Accueil.html/supevenement")
    public String suppEvenement(@ModelAttribute("evenement") Evenement evenement){
        Evenement e = evenementRepository.findById(evenement.getNumeven()).get();
        List<Participant> participants = e.getParticipants();
        participants.forEach((p) -> {
            participantRepository.delete(p);
        });
        evenementRepository.delete(e);
        return "redirect:/Accueil.html/evenements";
    }
    
    @GetMapping("/index")
    public String deconnexion(SessionStatus status, HttpSession session,  WebRequest request){ 
        status.setComplete();
        session.removeAttribute("connexion");
        return "index";
    }
}
