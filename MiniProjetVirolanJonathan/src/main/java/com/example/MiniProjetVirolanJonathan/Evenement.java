/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.MiniProjetVirolanJonathan;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author formation
 */
@Entity
@Table(name = "evenement")
public class Evenement {
    
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="num_even")
    private int numeven;
    
    @Column(name="intitule", nullable=false)
    private String intitule;
    
    @Column(name="theme", nullable=false)
    private String theme;
    
    @Column(name="date_debut", nullable=false)
    private String datedebut;
    
    @Column(name="duree", nullable=false)
    private String duree;
    
    @Column(name="nb_part_max", nullable=false)
    private int nbpartmax;
    
    @Column(name="description", nullable=false)
    private String description;
    
    @Column(name="organisateur", nullable=false)
    private String organisateur;
    
    @Column(name="type_even", nullable=false)
    private String typeeven;
    
    @Column(name="participants", nullable=false)
    @OneToMany(mappedBy = "evenement")
    private List<Participant> participants;
    
    public Evenement(){}

    public Evenement(String intitule, String theme, String datedebut, String duree, int nbpartmax, String description, String organisateur, String typeeven) {
        this.intitule = intitule;
        this.theme = theme;
        this.datedebut = datedebut;
        this.duree = duree;
        this.nbpartmax = nbpartmax;
        this.description = description;
        this.organisateur = organisateur;
        this.typeeven = typeeven;
        this.participants = new ArrayList<Participant>();
    }

    

    
    
    

    public int getNumeven() {
        return numeven;
    }

    public void setNumeven(int numeven) {
        this.numeven = numeven;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(String datedebut) {
        this.datedebut = datedebut;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public int getNbpartmax() {
        return nbpartmax;
    }

    public void setNbpartmax(int nbpartmax) {
        this.nbpartmax = nbpartmax;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganisateur() {
        return organisateur;
    }

    public void setOrganisateur(String organisateur) {
        this.organisateur = organisateur;
    }

    public String getTypeeven() {
        return typeeven;
    }

    public void setTypeeven(String typeeven) {
        this.typeeven = typeeven;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }
    
    public void addParticipant(Participant p){
        this.participants.add(p);
    }
}
