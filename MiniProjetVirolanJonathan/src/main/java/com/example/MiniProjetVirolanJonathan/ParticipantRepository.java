/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.MiniProjetVirolanJonathan;

import org.springframework.data.repository.CrudRepository;
/**
 *
 * @author formation
 */
public interface ParticipantRepository extends CrudRepository<Participant, Integer>{
    
}
